# PubSpec SetUp

There is one setting that is specifc to my flutter projects setups.

## Assets-Images

I set an images assets sub-folder for the goldens toolkit library I use:

```
assets:
    - images/

```

## Derry Setup

I also need the derry script dependency line at the end of the pubspec:

```

# required for derry scripts
scripts: derry.yaml

```