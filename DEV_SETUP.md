# DEV_SETUP

Basic dev setup settings foryour dev laptop or desktop to effectively use flutter sdk framework to develop flutter apps.

Organizational Note: Many of these tools need either an env var set or parth set. Often it's easier if one set's a named env varpointing to parth anyway as then it makes yoru path entries somewhat maintainable.

## Flutter IDE Setup

This is simply install community edition of IntelliJ or Beta Android Studio or Microsoft's Insiders edition of VSCode.


## DEV Tool Dependencies

### GIT

```
C:\Program Files\Git\cmd
C:\Program Files\Git\bin

```

### PLANTUML

```
PLANTUML D:\myOpt\plantuml\plantuml.jar
```

### LCOV-GENHTML
```
LOCOV_HOME C:\ProgramData\chocolatey\lib\lcov\tools
GENHTML C:\ProgramData\chocolatey\lib\lcov\tools\bin\genhtml
```

### Junit2HTML
```

```

## Flutter and Dart SDKs
```
D:\fluttersdk\myfluttersdk\flutter\bin\cache\dart-sdk\bin
D:\fluttersdk\myfluttersdk\flutter\.pub-cache\bin
D:\fluttersdk\myfluttersdk\flutter\bin

```

Note, once you set the dart and flutter paths correctly you will be able to install the dary binaries with a pub global activate 
command as the dart path setting puts the dart pub command on path.

### Dart Binary Dependencies

Once the flutter and dart sdks are properly installed and the paths are set it's simply using the pub tool to install and then it's simply typing the binary name in erminalto launch.

All my tutorial flutter repos use this tool set so you kind of need to install them to follow my tutorials.

#### DCDG
```
pub global activate dcdg
```

#### DARTDOC

```
pub global activate dartdoc
pub global activate dhttpd
```

You need the dhttpd dart binary to run the httpd server as the doc search only works that way due to cross-site-scripting warnings.

When project folder do:
```
dhttpd --path doc/api

```

And than your docs wilbe served at:

```
http://localhost:8080
```

#### DART CODE METRICS
```
pub global activate dart_code_metrics
```

To run is
```
dart_code_metrics:metrics lib
```

#### DERRY

```
pub global activate derry
```

#### JUNITREPORT

```
pub global activate junitreport
```

To run

```
tojunit
```


## Target Platform SDKs

### Android

It is easier to download the Android Studio IDE and use that GUI to download anddroid sdks and install them.

```
D:\androidstuff\androidsdk\emulator\emulator
D:\androidstuff\androidsdk\cmdline-tools\latest-2\bin
D:\androidstuff\androidsdk\platform-tools
D:\androidstuff\androidsdk\tools

ANDROID_SDK_ROOT D:\androidstuff\androidsdk
VM_SERVICE_URL http://127.0.0.1:8888/

```

### IOS

To install ios sdks one has to instal the XCode IDE from Apple.


### WEB-Browsers

It's real easy, install the browser and you are done!


## Run Configs

These are the run confgis for Android Studio or IntelliJ or VSCode.

### Android Studio or IntelliJ

The run config copies are in the dot-run folder along with a keyboard shortcut pdfs. Note, that oneof the run confgis has an aboslute path to a testing file that you need to change to use.

### MS's VSCode

The run confgis and the workspace settings are all in the dot-vscode folder along with keyboard shortcut pdfs. Note, that I included a recommeded list of plugins.


## Resources

Specific article resources to how to set this up:



