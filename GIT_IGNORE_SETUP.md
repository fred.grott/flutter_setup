# Git Ignore SetUp

I add one line to my dot-gitignore file to account for the specialized goldens toolkit library I use:

``` 
# don't check in golden failure output
**/failures/*.png

```

