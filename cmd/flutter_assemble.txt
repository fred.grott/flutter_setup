Assemble and build Flutter resources.

Global options:
-h, --help                  Print this usage information.
-v, --verbose               Noisy logging, including all shell commands executed.
                            If used with --help, shows hidden options.
-d, --device-id             Target device id or name (prefixes allowed).
    --version               Reports the version of this tool.
    --suppress-analytics    Suppress analytics reporting when this command runs.

Usage: flutter assemble [arguments]
-h, --help                            Print this usage information.
-d, --define                          Allows passing configuration to a target with --define=target=key=value.
    --performance-measurement-file    Output individual target performance to a JSON file.
-i, --input                           Allows passing additional inputs with --input=key=value. Unlike defines, additional inputs do not generate a new configuration, instead they are treated as dependencies of the targets that use them.
    --depfile                         A file path where a depfile will be written. This contains all build inputs and outputs in a make style syntax
    --build-inputs                    A file path where a newline separated file containing all inputs used will be written after a build. This file is not included as a build input or output. This file is not written if the build fails for any reason.
    --build-outputs                   A file path where a newline separated file containing all outputs used will be written after a build. This file is not included as a build input or output. This file is not written if the build fails for any reason.
-o, --output                          A directory where output files will be written. Must be either absolute or relative from the root of the current Flutter project.
    --ExtraGenSnapshotOptions         
    --ExtraFrontEndOptions            
    --DartDefines                     
    --resource-pool-size              The maximum number of concurrent tasks the build system will run.

Run "flutter help" to see global options.
