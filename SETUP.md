# SETUP
This is details on the setup of the dart-flutter sdk's , tool dependencies, and the 
flutter_setup project dependencies.
# Table Of Contents
1. [SDK SETUP](#SDK SETUP)
   1. [ANDROID SDK SETUP](#ANDROID SDK SETUP)
   2. [IOS SDK SETUP](#IOS SDK SETUP)
   3. [FLUTTER SDK SETUP](#FLUTTER SDK SETUP)
2. [IDE SETUP](#IDE SETUP)
   1. [ANDROID STUDIO SETUP](#ANDROID STUDIO SETUP)
   2. [MS VSCODE SETUP](#MS VSCODE SETUP)
3. [TOOL DEPENDENCIES SETUP](#TOOL DEPENDENCIES SETUP)
   1. [LCOV SETUP](#LCOV SETUP)
   2. [JUNIT2HTML PYTHON SCRIPT SETUP](#JUNIT2HTML PYTHON SCRIPT SETUP)
   3. [PLANTUML SETUP](#PLANTUML SETUP)
4. [DART BINARY TOOL SETUP](#DART BINARY TOOL SETUP)
   1. [DEVTOOLS](#DEVTOOLS)
   2. [DCDG](#DCDG)
   3. [DART CODE METRICS](#DART CODE METRICS)
   4. [DERRY](#DERRY)
   5. [DARTDOC](#DARTDOC)
   6. [DHTTPD](#DHTTPD)
   7. [JUNITREPORT](#JUNITREPORT)
4. [FLUTTER_SETUP OPERATIONS](#FLUTTER_SETUP OPERATIONS)
   1. [RUN CONFiGS](#RUN CONFIGS)
   2. [LINT SETUP](#LINT SETUP)
   3. [WORKFLOW AUTOMATION](#WORKFLOW AUTOMATION)
   4. [COMMAND LINE](#COMMAND LINE)
   5. [DOMAIN DRIVEN ARCHITECTURE](#DOMAIN DRIVEN ARCHITECTURE)
   6. [WINDOWS QUIRKS](#WINDOWS QUIRKS)
## SDK SETUP
Let's make sure you have the sdk setup right. You need as of 2021 at least one mobile sdk as 
the web platform support is still beta, so let's load at least onemobile sdk
### ANDROID SDK SETUP
The easiest way to install mobile sdk's is to install the companion IDE of that mobile sdk. 
In android's case it's the android studio ide:

[android studio  download](https://developer.android.com/studio)

In the android sdk world, the android studio preview(beta) is the one that keeps up-to-date with both 
new android studio flutter plugins AND the new android sdk releases per OS version and new emulator 
versions. Also note, that Android Studio Canary never ever will be able to run the flutter plugin, 
so get the android studio beta version in the preview tab on the left side of the screen that comes up.

When you install android studio it will automatically download one android sdk. Right now flutter 
supports Orero(8.1) all the way to android OS version 12. So download the lower sdks.

The anv variables to set are:
```
ANDROID_SDK_ROOT
```
And the path settings to set are:
```
path to platforms folder in android sdk install
path to tools folder in android sdk install
path to the platfomrs-tools folder in android sdk install
```
If on MS Windows set Windows Defender Virus File scanner to exclude the folder you installed 
the android sdk into and make sure you did not install the android sdk into asfolder with a 
space in the title of the folder.

## IOS SDK SETUP

Just as easy as the android sdk install, download Apple's XCode IDE as the ios sdk download is 
packaged with that IDE. And the  env you need to set is the:
```
SDKROOT = /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/IOSNumber.Number.sdk
```
I am not on Apple prodcuts yet(Feb2021) but am told that XCode auto sets it when you donwload the first sdk.

## FLUTTER SDK SETUP
 At bare minimum Flutter SDK will want to make sure one SDK is set when you issue the 
```
flutter doctor
```
command in the terminal so let's verify that you have at least one mobile sdk installed and the 
proper env settings. On Unix and Mac, the terminal command to output envs is:
```
printenv
```
and to print out the value of the one single varaible is 
```
echo $varname 
```
and so to do it for the android sdk it's
```
echo $ADNROID_SDK_ROOT
```
and for on Mac for the ios sdk it's
```
echos $SDKROOT
```
If you get a null or zero value that means it's not set and you need to set that env variable.

The Flutter SDK download is at

[FLUTTER SDK DOWNLOAD](https://flutter.dev/docs/get-started/install)

you unzip your download into a folder that does not have a space in the name. If you are on 
MS Windows you need to exclude that folder from your virus scanning software. Now we are at the 
set paths part:
```
path to flutter/bin
path to flutter/.pub-cache/bin
```
The flutter docs has this still wrong as since flutter sdk now includes the dart sdk the correct pat to 
dart is flutter/bin and correct path to pub-cache is that one.

Now since these are paths you need to print out the path, on  Unix and Mac:
```
echo $PATH
```
On MS Windows it's:
```
$Env:Path
```
and yes I gave you the powershell syntax as no one is using the cmd syntax anymore.

Now you last step for the flutter sdk install is to run flutter doctor:
```
flutter doctor
```
and you will get an output and on that screen do the ys accept android licenses and make sure it displays 
at least one sdk detected.

## IDE SETUP
I am just going to set through some quirks in these IDE setups as most of it's automated when you do the 
auto install.

### ANDROID STUDIO SETUP
As I stated before you do not want the canary version of Android Studio but the beta varsion as that 
keeps up with new android studio flutter plugins and the android emulators. The -plugins you want 
installed are:
1. Flutter as it also propmpts you to install the dart plagin
2. PlantUML
3. Markdown
4. Editorconfig



### MS VSCODE SETUP
To install MS VSCODE:

[MS VSCODE Download](https://code.visualstudio.com/)

And you want the same exact plugins as android studio, ie:
1. Flutter as it also propmpts you to install the dart plaugin
2. PlantUML
3. Markdown
4. Editorconfig

The only difference between VScode and Android Studio is that you need two other plugins, namely 
one for the ios emulator and one for the android emulator:
5. Android Emulator
6. iOS Emulator

And that is it.

## TOOL DEPENDENCIES SETUP
You have two tool dependency groups to setup. This is the non-dart tool dependency set that you need 
to setup.
### LCOV SETUP
The code coverage of widget testing is basically nicknamed lcov and is typcially named lcov.info 
as the output the analysis tool produces. That can be viewed in several 3rd party online tools.
But, you should be able to view it locally. You can download a set of perl scripts in a package called 
lcov and the script we use is called genhtml out of that package.

Getting it installed with it's dependencies is easy no matter if you are on mac, Linux-Unix, or 
MS Windows as you just use yoru package manager and it takes care of locating and downloading the 
correct version of perl to enable the lcov package to correctly work out-of-box.

Once you do that than you put the path to the genhtml perl script in an env variable named GENHTML. 
On MS Windows since you used the Chocolately package manager it's this:
```
GENHTML = C:\ProgramData\chocolatey\lib\lcov\tools\bin\genhtml
```

Last step, download my lcov.info file from this project as it's in the reports/coverage folder. Now put the locv.info 
file you downloaded to a spearate folder and open your terminal and type:
```
genhtml lcov.info
```
and you should see in your folder a folder named html with the htmlreprots inside.
### JUNIT2HTML PYTHON SCRIPT SETUP
It's different than the LCOV installation as  we do not have an option to install JUNIT2HTML python 
script under the ooperating System package managers. In this case no matter what operating system 
you use and what package manaer you use, you will use your package manager to install 
python first and than use the PIP python packager maanager to install JUNIT2HTML via:
```
pip install junit2jtml
```

And once you have installed both python and junit2html you will need to verify that it's 
working via taking the xml file in this project's reports/test folder and typing 
```
junit2html test-reportname.xml text-reportname.html
```
and you should notice and html file generated with that command.
### PLANTUML SETUP
We use the plantuml jar to generate the png image of our UML diagrams of our flutter-dart code.
And the download of the plantuml jar is at:

[plantuml jar download](https://plantuml.com/download)

You will want to set an env variable that stores the path to the jar:
```
PLANTUML = path to jar
```
Next, you will download a jdk as plantuml jar needs to use that to run the jar application.
You can download a java jdk at Amazon via:

[Amazon OpenJDK Download](https://aws.amazon.com/corretto/)

You do not need jdk 15 or jdk 11 just get jdk 8.  And than set an env variable of 
```
JAVA_HOME = path to the OpenJDK you just downloaded
```
Download the uml puml file from this project's reports/uml folder as you will use that to 
verify that you installed both plantuml jar and  the jdk correctly. Open the terminal in that 
folder in which you downloaded the puml file and type this:
```
java -jar $PLANTUML karma.puml
```
If on MS Windows replace the 
```
$PLANTUML
```
with this instead
```
%PLANTUML%
```

and you should get an output of karma.png
## DART BINARY TOOL SETUP
These are the dart binary tools that we will use to make developing flutter apps easier. Each of these 
installs to the global_packages ubs-folder of the pub-cache folder.
### DEVTOOLS
Devtools can be found at:

[devtools@pub.dev](https://pub.dev/packages/devtools/install)

Because we use devtools during integration testing we need to install this dart binrary by typing 
this in the terminal:
```
pub global activate devtools
```
### DCDG
DCDG is the dart binary that creates our uml  diagrams in puml format that we than use plantuml to 
get those visual png diagrams. Ity can be installed by typing this in the terminal:
```
pub global activate dcdg
```
### DART CODE METRICS
Dart Code Metrics dart binary is at:

[Dart Code Metrics@pub.dev](https://pub.dev/packages/dart_code_metrics)

It can be installed by typing this into your terminal:
```
pub global activate dart_code_metrics
```
This is one dart binary that requires some extra dart dependencies defined in the project 
pubspec which is already in this project's pubspec and the lint rules added to the 
project's analysis_options yaml file.

### DERRY
Derry is the workflow automation stuff that is being used in this project. The Derry binary is 
at:

[Derry@pub.dev](https://pub.dev/packages/derry)

It can be installed by typing this into your terminal:
```
pub global activate derry
```

### DARTDOC
Dartdoc is the doc generation tool for dart and flutter to document apis. It can be found at:

[DARTDOC@pub.dev](https://pub.dev/packages/dartdoc)

It can be installed by typing this into your terminal:
```
pub global activate dartdoc
```
### DHTTPD
Dhttpd is the server we use to view the api docs and have the search tool work. It can be 
found at:

[DHTTPD@pub.dev](https://pub.dev/packages/dhttpd)

It can be installed by typing this into your terminal:
```
pub global activate dhttpd
```
### JUNITREPORT
JunitReport is used to get the widget tests into a form for CI server consumption. It can 
be found at:

[JUNITREPORT@pub.dev](https://pub.dev/packages/junitreport)

It can be installed by typing this into your terminal:
```
pub global activate junitreport
```

As a last check you should have in this sub-folder in the flutter sdk:
```
pub-cache/global-packages
```
seven folders denoting the dart binaries you just installed.
## FLUTTER_SETUP OPERATIONS
If you have did everything thus far than you are set to understand how this project can get you into 
the grove of faster process of learning flutter due to having immediate feedback at hand during your 
app  design and app code sprints.

### RUN CONFIGS
In the .run sub-folder you will find run configurations:
1. dontrun-integration-test.run.xml is the configuration that you run after starting the app on 
device or emulator to run integratin tests.
2. main-dart.xml is the main one to run the app
3. tests in widget-test.dart.run.xml is the one to run widget tests

In your own project you will change these to point to the correct corresponding target file.
### LINT SETUP
Let me state in my lint setup that I am a bit contrarian in that i want the rules viewable that 
I am subjecting my code to in my analysis_options yaml file so I do not apply lint rule plugins 
to the analysis_options file but instad copy and paste the rules I am applying.

If you want to use a different set of lint rules, no harm done. Take mine out and replace with the 
lint rule set you want to use.

Also, in this lint setup I am also using the lint rules form the code metrics plugin.

### WORKFLOW AUTOMATION
The workflow automation script using Derry is already somewhat figured out. If you follow comments in 
the script itself you can see how to use it.

### COMMAND LINE
A large part of the command line stuff that you need for flutter app development is handled by the 
workflow automation.  Namely, running tests, code metrics reports, api doc generation, etc.

### DOMAIN DRIVEN ARCHITECTURE
I structure my flutter app architecture using layers. You might think of it as an Onion. The center 
layer is the domain which contains those functions and widgets that have no UI dependencies,ie they 
are to be considered pure functionsand widgets. The presentation layer is considered to be the outer-most 
layer and has only non-pure functions and widgets that have Ui dependencies.

You will find that I already have sub-folders defined in the lib folder for this set-uo.

### WINDOWS QUIRKS
MS Windows has some quirks that you need to be aware of to properly develop flutter apps if you use it as 
your laptop or desktop OS when developing. This is the settings you need to change and make:
1. Turn off the auto folder protection for the programs folder.
2. In your virus software, change the settings so that all your sdk 
folders are not scanned.
3. Right-click your IDE shortcut and click on properties. On that screen click on the compatibility 
tab and click the run with admin account check-box.
4. Even than you will still have to run the drive portion of integration test via the 
command line with the command of:
```
flutter drive --driver=test_driver/integration_test.dart --target=integration_test/widget_screen_test.dart --host-vmservice-port 8888 > machine.log
```